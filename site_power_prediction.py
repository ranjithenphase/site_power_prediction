import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn import preprocessing, svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from matplotlib import style
import datetime
import mpl_toolkits
%matplotlib inline

data = pd.read_csv("badri_rollups.csv")
#data = data.reindex(index=data.index[::-1])
df = data.loc[:, ('energy_produced','peak_power','date')]

df.head()

df.info()

df.describe()

plt.scatter(df.peak_power, df.energy_produced)
plt.title("Peak Power vs Energy Production")
plt.show()

forecast_col = "energy_produced"
df.fillna(-99999, inplace=True)
forecast_out=int(math.ceil(0.01 * len(df)))
print(len(df), forecast_out)
#print(forecast_out)
df['label'] = df[forecast_col].shift(-forecast_out)

df.dropna(inplace=True)
df.head()

X = np.array(df.drop(['label','date'], 1))
y = np.array(df['label'])
X = preprocessing.scale(X)
#X = X[:-forecast_out]
#X_lately = X[-forecast_out:]

y = np.array(df['label'])
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
reg = LinearRegression()
reg.fit(X_train,y_train)
accuracy = abs(reg.score(X_test,y_test))
print(accuracy)
forecast_set = reg.predict(X_test)
print(forecast_set)
print(forecast_out)


last_day = datetime.datetime.strptime(df.iloc[-1].date, "%Y/%m/%d")
last_unix = last_day.timestamp()
print(last_unix)
#next_day = last_day + datetime.timedelta(days=1)
one_day = 86400
next_unix = last_unix + one_day
print(next_unix)

df['Forecast'] = np.nan

for i in forecast_set:
    next_date = datetime.datetime.fromtimestamp(next_unix)
    next_unix += 86400
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)]+[i]

#print(df.tail())   
style.use('ggplot')

df['energy_produced'].plot()
df['Forecast'].plot()
plt.legend(loc=4)
plt.xlabel('Date')
plt.ylabel('Production')
plt.show()

